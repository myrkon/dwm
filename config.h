#include "movestack.c"
#include <stdlib.h>
/* See LICENSE file for copyright and license details. */

/* appearance */
static const int title_shift        = 15;        /* spacing between tags and title */
static const int user_bh            = 32;        /* bar height */
static const int vertpad            = 10;        /* vertical dwmbar padding */
static const int sidepad            = 0;        /* dwmbar padding from sides*/
static unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static int smartgaps                = 0;        /* 1 means no outer gap when there is only one window */
static const int focusonwheel        = 0;
static unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;     /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 5;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static int showsystray        = 0;     /* 0 means no systray */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static char *fonts[]          = { 
  "FontAwesome:size=11",
  "Cozette:size=11",
};
static char dmenufont[]       = "Cozette:size=11";
// catppuccin theme
static char normbgcolor[]       = "#282828"; // normal bg
static char normbordercolor[]       = "#a89984"; // normal border
static char normfgcolor[]       = "#fbf1c7"; // normal fg
static char selfgcolor[]       = "#32302f"; // selected fg
static char selbgcolor[]        = "#8ec07c"; // accent
static char *colors[][3]      = {
  /*               fg         bg         border   */
  [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
  [SchemeSel]  = { selfgcolor, selbgcolor,  selbgcolor  },
  [SchemeStatus]  = { normfgcolor, normbgcolor,  "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
  [SchemeTagsSel]  = { selfgcolor, selbgcolor,  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
  [SchemeTagsNorm]  = { normfgcolor, normbgcolor,  "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
  [SchemeInfoSel]  = { normfgcolor, normbgcolor,  "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
  [SchemeInfoNorm]  = { normfgcolor, normbgcolor,  "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4" };

static const Rule rules[] = {
  /* class            instance    title       tags mask     isfloating   canfocus    isalwaysontop  monitor float x,y,w,h  floatborderp */
  // floating
  { "Kotatogram",     NULL,       NULL,       0,            1,           1,           0,              -1,     452,325,452,700,    1 },
  { "mpv",            NULL,       NULL,       0,            1,           1,           0,              -1,     0,0,854,480,    2 },
  { NULL,             NULL,       "Picture in Picture", 0,  1,           1,           0,              -1,     0,0,500,500,    2 },
  { NULL,             NULL,       "Event Tester", 0,        1,           1,           0,              -1,     0,0,200,200,    0 },
  // pinned to specefic tags
  { NULL, "microsoft-edge",       NULL,       1 << 1,       0,           1,           0,              -1,     0,0,0,0,        0 },

  // no focus, no borders
  { "Polybar",        NULL,       NULL,       0,            0,           0,           1,              -1,     0,0,0,0, 0 },
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
  /* symbol     arrange function */
  { "[]=",      tile },    /* first entry is default */
  { "><>",      NULL },    /* no layout function means floating behavior */
  { "|M|",      centeredmaster },
  { ">M>",      centeredfloatingmaster }, // i can delete this later
  { "[M]",      monocle },
//  { "[@]",      spiral },
//  { "[\\]",     dwindle },
//  { "H[]",      deck },
//  { "TTT",      bstack },
//  { "===",      bstackhoriz },
//  { "HHH",      grid },
//  { "###",      nrowgrid },
//  { "---",      horizgrid },
//  { ":::",      gaplessgrid },
//  { "|M|",      centeredmaster },
//  { ">M>",      centeredfloatingmaster },
  { NULL,        NULL},
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
  { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
  { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
  { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
  { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *browsercmd[] =  {"/bin/sh", "-c", "microsoft-edge-stable"};
static const char *telegramcmd[] =  {"/usr/bin/kotatogram-desktop"};
// maim -s | xclip -selection clipboard -t image/png
static const char *scrshtcmd[] = { "/bin/sh", "-c", "maim -s | xclip -selection clipboard -t image/png" };

 /* Xresources preferences to load at startup */
ResourcePref resources[] = {
    { "dmenufont",          STRING,  &dmenufont },
    { "normbgcolor",        STRING,  &normbgcolor },
    { "normbordercolor",    STRING,  &normbordercolor },
    { "normfgcolor",        STRING,  &normfgcolor },
    { "selbgcolor",         STRING,  &selbgcolor },
    { "selfgcolor",         STRING,  &selfgcolor },
    { "borderpx",            INTEGER, &borderpx },
    { "snap",                INTEGER, &snap },
    { "showbar",            INTEGER, &showbar },
    { "topbar",              INTEGER, &topbar },
    { "nmaster",            INTEGER, &nmaster },
    { "resizehints",         INTEGER, &resizehints },
    { "mfact",           FLOAT,   &mfact },
};


void 
changevolume(const Arg* arg) {
  if (arg->i > 0) {
    system("pamixer -i 5 &");
  } else if (arg->i < 0) {
    system("pamixer -d 5 &");
  } else {
    system("pamixer -t &");
  }
}

static Key keys[] = {
  /* modifier                     key        function        argument */
  // apps
  { MODKEY,                       XK_r,      spawn,          {.v = dmenucmd } },
  { MODKEY,                       XK_d,      spawn,          {.v = telegramcmd } },
  { MODKEY,                       XK_s,      spawn,          SHCMD("microsoft-edge-stable") },

  // media
  { MODKEY,                       XK_c,      spawn,          SHCMD("maim -s | xclip -selection clipboard -t image/png") },
  { MODKEY,                       XK_minus,  changevolume,   {.i = -1} },
  { MODKEY,                       XK_equal,  changevolume,   {.i = 1} },
  { MODKEY,                       XK_0,      changevolume,   {.i = 0} },

  // window-related stuff
  { MODKEY,                       XK_a,      spawn,          {.v = termcmd } },
  { MODKEY,                       XK_b,      togglesystray,  {0} },
  { MODKEY,                       XK_j,      focusstack,  {.i = +1 } },
  { MODKEY,                       XK_k,      focusstack,  {.i = -1 } },
  { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
  { MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = -1 } },
  { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
  { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
  { MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
  { MODKEY,                       XK_Tab,    view,           {0} },
  { MODKEY,                        XK_w,     killclient,     {0} },
  { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
  { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
  { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
  { MODKEY,                       XK_space,  cyclelayout,    {.i = +1} },
  { MODKEY|ShiftMask,             XK_space,  cyclelayout,    {.i = -1} },
  { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
  { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
  { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
  { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
  { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
  { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
  { MODKEY,                        XK_o,     togglealwaysontop, {0} },
  { MODKEY|ControlMask,            XK_m,     togglefloating, { 0 } },
  { MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
  { MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
  { MODKEY,                        XK_g,     incrgaps,       {.i = +2 } },
  { MODKEY|ShiftMask,              XK_g,     incrgaps,       {.i = -2 } },
  { MODKEY|ControlMask,            XK_g,     togglegaps,     {0} },
  { MODKEY|ShiftMask|ControlMask,  XK_g,     defaultgaps,     {0} },
  TAGKEYS(                        XK_1,                      0)
  TAGKEYS(                        XK_2,                      1)
  TAGKEYS(                        XK_3,                      2)
  TAGKEYS(                        XK_4,                      3)
  { MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click                event mask      button          function        argument */
  { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
  { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
  { ClkWinTitle,          0,              Button2,        zoom,           {0} },
  { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
  { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
  { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
  { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
  { ClkTagBar,            0,              Button1,        view,           {0} },
  { ClkTagBar,            0,              Button3,        toggleview,     {0} },
  { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
  { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

