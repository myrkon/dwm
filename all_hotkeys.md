mod+r - dmenu_run
mod+a - terminal
mod+s - telegram
mod+d - browser

mod+b - toggle bar // X

mod+j - focus left or change size
mod+k - focus right or change size
mod+h - focus
mod+l - focus

mod+i - increase number of master windows
mod+shift+i - decrease number of master windows

mod+shift+return - zoom (as i think, move focused window to the master

mod+tab - tab to last tag // X

mod+w - kill window

mod+t - set layout 0 // X
mod+f - set layout 1 // X
mod+m - set layout 2 // X

mod+space - cylce layouts next
mod+shift+space - cycle layouts prev // X

mod+0 - view all windows on the monitor
mod+shift+0 - show focues app on all tags

mod+comma - go to next monitor
mod+period - go to previous monitor
mod+shift+comma - move window to next monitor
mod+shift+period - move window to prev monitor


mod+o - always on top

mod+shift+j\k - move stack

mod+g - increase gaps
mod+shift - decrease gaps
mod+ctrl+g - toggle gaps
mod+shift+ctrl+g - default gaps
